Feature: Admin User Manipulates Person's Titles
  As an Admin User
  I need to be able to add and remove titles from people
  So they can be properly recognized for their accomplishments in life

  Background:
    Given I am an admin user
    And I am logged in

    @javascript
  Scenario: Add a title to a person
    Given I have a person
    And I am at the "/admin/people" path
    When I click on "Edit"
    And I give the title "Biologist" in slot 1
    And I click the "Update Person" button
    Then my person should have 1 titles
    And I should see the title "Biologist"

  Scenario: Saving a person with a title doesn't duplicate the title
    Given I have a person with a title "Biologist"
    When I am at the "/admin/people/1/edit" path
    And I click the "Update Person" button
    Then my person should have 1 titles
    And I should see the title "Biologist"

    @javascript
  Scenario: Add a title to a person who already has one
    Given I have a person with a title "Epidemiological Investigator"
    When I am at the "/admin/people/1/edit" path
    And I give the title "Epidemiologist" in slot 2
    And I click the "Update Person" button
    Then my person should have 2 titles
    And I should see the title "Epidemiological Investigator"
    And I should see the title "Epidemiologist"

  Scenario: Delete a title on a person
    Given I have a person with a title "Epidemiological Investigator"
    When I am at the "/admin/people/1/edit" path
    And I check "Delete"
    And I click the "Update Person" button
    Then my person should have 0 titles
    And I should not see the title "Epidemiological Investigator"
