Feature: Admin User Manipulates Person's Addresses
  As an Admin User
  I need to be able to create, change, and remove addresses from people
  So that people can be physically found

  Background:
    Given I am an admin user
    And I am logged in
    And I have a person

    @javascript
  Scenario: Add an address to a person
    Given I am at the "/admin/people/1/edit" path
    When I give valid information for a new address
    And I click the "Update Person" button
    Then my person should have an address
    And I should see the valid address information

    @javascript
  Scenario: Add a second address to a person
    Given my person has an existing address
    And I am at the "/admin/people/1/edit" path
    When I give valid information for a second address
    And I click the "Update Person" button
    Then my person should have 2 addresses
    And I should see the existing address
    And I should see the second address

  Scenario: Delete an address from a person
    Given my person has an existing address
    And I am at the "/admin/people/1/edit" path
    When I check "Delete" in the address
    And I click the "Update Person" button
    Then my person should have 0 addresses
    And I should not see the existing address

