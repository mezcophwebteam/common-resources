Feature: Admin User logs in
  As an admin for people
  I need to be able to log in
  So I can edit information

  Scenario: Log in
    Given I am an admin user
    And I am at the log in page
    When I give valid login information
    And I click the "Login" button
    Then I should be at the admin dashboard
