Feature: Admin User Manipulates Basic People Data
  As an admin user
  I need to properly click buttons and fill out forms
  So I can administer the people

  Background:
    Given I am an admin user
    And I am logged in

  Scenario: Go to people index listing
    When I click on "People"
    Then I should be at the "/admin/people" path

  Scenario: Create a new person
    Given I am at the "/admin/people" path
    When I click on "New Person"
    And I give valid person information
    And I click the "Create Person" button
    Then I should have a new person

    @javascript
  Scenario: Delete a person
    Given I have a person
    And I am at the "/admin/people" path
    When I click on "Delete" and accept the resulting confirmation
    Then I should have no people
