Feature: Admin User Manipulates Person's Divisions
  As an Admin User
  I need to be able to assign people to divisions
  So they can be retrieved in collections later

  Background:
    Given I am an admin user
    And I am logged in
    And I have a person

  Scenario: Set a person's division
    Given I am at the "/admin/people/1/edit" path
    When I select the "Community, Environment & Policy" division
    And I click the "Update Person" button
    Then I should see the "Community, Environment & Policy" division

  Scenario: Unset a person's division
    Given my person has the division "Health Promotion Sciences"
    And I am at the "/admin/people/1/edit" path
    When I select the "" division
    And I click the "Update Person" button
    Then I should see no division
    
  Scenario: Change a person's division
    Given my person has the division "Phoenix Campus"
    And I am at the "/admin/people/1/edit" path
    When I select the "Epidemiology and Biostatistics" division
    And I click the "Update Person" button
    Then I should see the "Epidemiology and Biostatistics" division
