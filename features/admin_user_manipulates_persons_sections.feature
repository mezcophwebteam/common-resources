Feature: Admin User Manipulates Person's Sections
  As an Admin User
  I need to give people primary and secondary sections
  So they can be classified for retrieval

  Background:
    Given I am an admin user
    And I am logged in

  Scenario: Set person's primary section
    Given I have a person
    And I am at the "/admin/people/1/edit" path
    When I select the "Epidemiology" primary section
    And I click the "Update Person" button
    Then I should see the "Epidemiology" primary section

  Scenario: Unset a person's primary section
    Given I have a person with a primary section "Epidemiology"
    And I am at the "/admin/people/1/edit" path
    When I select the "" primary section
    And I click the "Update Person" button
    Then I should see no primary section

  Scenario: Give a person a section
    Given I have a person with a primary section "Epidemiology"
    And I am at the "/admin/people/1/edit" path
    When I check the "Biostatistics" section
    And I click the "Update Person" button
    Then I should see "Biostatistics" listed under sections

  Scenario: Give a person multiple sections
    Given I have a person with a primary section "Epidemiology"
    And I am at the "/admin/people/1/edit" path
    When I check the "Biostatistics" section
    And I check "Family and Child Health"
    And I click the "Update Person" button
    Then I should see "Biostatistics" listed under sections
    And I should see "Family and Child Health" listed under sections

  Scenario: Remove a section from a person
    Given I have a person with the primary section "Epidemiology" and a section "Biostatistics"
    And I am at the "/admin/people/1/edit" path
    When I uncheck the "Biostatistics" section
    And I click the "Update Person" button
    Then I should see no section
    And I should not see "Biostatistics"

