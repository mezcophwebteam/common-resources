Given(/^I am an admin user$/) do
  AdminUser.create!(email: 'admin@example.com', password: 'password',
                    password_confirmation: 'password')
end

Given(/^I am at the log in page$/) do
  visit new_admin_user_session_path
end

When(/^I give valid login information$/) do
  fill_in 'Email', with: 'admin@example.com'
  fill_in 'Password', with: 'password'
end

When(/^I click the "(.*?)" button$/) do |text|
  click_button text
end

Then(/^I should be at the admin dashboard$/) do
  expect(current_path).to eq admin_root_path
end

Given(/^I am logged in$/) do
  step "I am at the log in page"
  step "I give valid login information"
  step "I click the \"Login\" button"
end

When(/^I click on "(.*?)"$/) do |text|
  click_on text
end

Then(/^I should be at the "(.*?)" path$/) do |path|
  expect(current_path).to eq path
end

Given(/^I am at the "(.*?)" path$/) do |path|
  visit path
end

When(/^I give valid person information$/) do
  fill_in 'Netid', with: 'jillvalentine'
  fill_in 'First name', with: 'Jill'
  fill_in 'Last name', with: 'Valentine'
  select 'Faculty', from: 'Employee type'
end

Then(/^I should have a new person$/) do
  expect(current_path).to eq admin_person_path(Person.last.id)
end

Given(/^I have a person$/) do
  step "I am at the \"/admin/people\" path"
  step "I click on \"New Person\""
  step "I give valid person information"
  step "I click the \"Create Person\" button"
  @person = Person.find_by!(netid: 'jillvalentine')
end

When(/^I give the title "(.*?)" in slot (\d+)$/) do |title_name, slot|
  within 'li.titles' do
    click_on "Add New Title"
    within "li#person_titles_attributes_#{slot.to_i - 1}_title_input" do
      fill_in 'Title', with: title_name
    end
  end
end

Then(/^my person should have (\d+) titles$/) do |titles_count|
  expect(@person.titles.count).to eq titles_count.to_i
end

Given(/^I have a person with a title "(.*?)"$/) do |title_name|
  step "I have a person"
  @person.titles.create(title: title_name)
end

Then(/^I should see the title "(.*)?"$/) do |title_name|
  expect(page).to have_content title_name
end

When(/^I click on "(.*?)" and accept the resulting confirmation$/) do |text|
  accept_confirm do
    click_on text
  end
end

Then(/^I should have no people$/) do
  expect(page).to have_content "There are no People yet."
end

When(/^I check the "(.*?)" section$/) do |checkbox|
  within "li#person_sections_input" do
    check checkbox
  end
end

When(/^I uncheck the "(.*?)" section$/) do |checkbox|
  within "li#person_sections_input" do
    uncheck checkbox
  end
end

When(/^I check "(.*?)"$/) do |checkbox|
  check checkbox
end

When(/^I uncheck "(.*?)"$/) do |checkbox|
  uncheck checkbox
end

Then(/^I should not see the title "(.*?)"$/) do |title|
  expect(page).to_not have_content title
end

When(/^I select the "(.*?)" primary section$/) do |section|
  select section, from: 'Primary section'
end

Then(/^I should see the "(.*?)" primary section$/) do |section_name|
  within '.row-primary_section' do
    expect(page).to have_content section_name
  end
end

Given(/^I have a person with the primary section "(.*?)" and a section "(.*?)"$/) do |primary_section, section|
  step "I have a person"
  visit edit_admin_person_path(@person)
  step "I select the \"#{primary_section}\" primary section"
  step "I check the \"#{section}\" section"
  step "I click the \"Update Person\" button"
end

Given(/^I have a person with a primary section "(.*?)"$/) do |section_name|
  step "I have a person"
  visit edit_admin_person_path(@person)
  step "I select the \"#{section_name}\" primary section"
  step "I click the \"Update Person\" button"
end

Then(/^I should see no primary section$/) do
  within '.row-primary_section' do
    expect(page).to have_content "Empty"
  end
end

Then(/^I should not see "(.*?)"$/) do |text|
  expect(page).to_not have_content text
end

Then(/^I should see no section$/) do
  within '.row-sections' do
    expect(page).to have_content "Empty"
  end
end

Then(/^I should see "(.*?)" listed under sections$/) do |section_name|
  within '.row-sections' do
    expect(page).to have_content section_name
  end
end

When(/^I select the "(.*?)" division$/) do |division_name|
  select division_name, from: "Division"
end

Then(/^my person should have a division$/) do
  expect(@person.division).to_not eq nil
end

Then(/^I should see the "(.*?)" division$/) do |division_name|
  within '.row-division' do
    expect(page).to have_content division_name
  end
end

Given(/^my person has the division "(.*?)"$/) do |division_name|
  @person.division = Division.find_by(name: division_name)
end

Then(/^I should see no division$/) do
  within '.row-division' do
    expect(page).to have_content "Empty"
  end
end

When(/^I check the research area "(.*?)"$/) do |research_area|
  within 'li#person_research_areas_input' do
    check research_area
  end
end

Then(/^I should see the "(.*?)" research area$/) do |research_area|
  within '.row-research_areas' do
    expect(page).to have_content research_area
  end
end

Given(/^my person has the research area "(.*?)"$/) do |research_area_name|
  @person.research_areas << ResearchArea.find_by(name: research_area_name)
end

When(/^I uncheck the research area "(.*?)"$/) do |research_area|
  within 'li#person_research_areas_input' do
    uncheck research_area
  end
end

Then(/^I should not see the "(.*?)" research area$/) do |research_area_name|
  within '.row-research_areas' do
    expect(page).to_not have_content research_area_name
  end
end

When(/^I give valid information for a new address$/) do
  @address = FactoryGirl.attributes_for(:address_for_awochna)
  click_on 'Add New Address'
  fill_in 'Street address', with: @address[:street_address]
  fill_in 'Building name', with: @address[:building_name]
  fill_in 'Room number', with: @address[:room_number]
  fill_in 'City', with: @address[:city]
  fill_in 'State', with: @address[:state]
  fill_in 'Zip', with: @address[:zip]
  fill_in 'Phone', with: @address[:phone]
end

Then(/^my person should have an address$/) do
  expect(@person.addresses.count).to eq 1
end

Then(/^I should see the valid address information$/) do
  within '.row-addresses' do
    expect(page).to have_content @address[:street_address]
    expect(page).to have_content @address[:building_name]
    expect(page).to have_content @address[:room_number]
    expect(page).to have_content @address[:city]
    expect(page).to have_content @address[:state]
    expect(page).to have_content @address[:zip]
    expect(page).to have_content @address[:phone]
  end
end

Given(/^my person has an existing address$/) do
  @person.addresses.create!(FactoryGirl.attributes_for(:address_for_jehiri))
end

When(/^I give valid information for a second address$/) do
  context = '#person_addresses_attributes_1_'
  @address ||= FactoryGirl.attributes_for(:address_for_awochna)
  click_on 'Add New Address'
  within context + 'street_address_input' do
    fill_in 'Street address', with: @address[:street_address]
  end
  within context + 'building_name_input' do
    fill_in 'Building name', with: @address[:building_name]
  end
  within context + 'room_number_input' do
    fill_in 'Room number', with: @address[:room_number]
  end
  within context + 'city_input' do
    fill_in 'City', with: @address[:city]
  end
  within context + 'state_input' do
    fill_in 'State', with: @address[:state]
  end
  within context + 'zip_input' do
    fill_in 'Zip', with: @address[:zip]
  end
  within context + 'phone_input' do
    fill_in 'Phone', with: @address[:phone]
  end
end

Then(/^my person should have (\d+) addresses$/) do |number|
  expect(@person.addresses.count).to eq number.to_i
end

Then(/^I should see the existing address$/) do
  within '.row-addresses' do
    expect(page).to have_content @person.addresses.first.street_address
    expect(page).to have_content @person.addresses.first.building_name
    expect(page).to have_content @person.addresses.first.room_number
    expect(page).to have_content @person.addresses.first.city
    expect(page).to have_content @person.addresses.first.state
    expect(page).to have_content @person.addresses.first.zip
    expect(page).to have_content @person.addresses.first.phone
  end
end

Then(/^I should see the second address$/) do
  step "I should see the valid address information"
end

When(/^I check "(.*?)" in the address$/) do |checkbox|
  within 'li.addresses' do
    check checkbox
  end
end

Then(/^I should not see the existing address$/) do
  within '.row-addresses' do
    expect(page).to_not have_content '1295 N. Martin Ave.'
    expect(page).to_not have_content 'Drachman Hall'
    expect(page).to_not have_content 'A256'
    expect(page).to_not have_content 'Tucson'
    expect(page).to_not have_content 'AZ'
    expect(page).to_not have_content '85724'
    expect(page).to_not have_content '5206261355'
  end
end

Then(/^I should see research areas$/) do
  within '.row-research_areas' do
    expect(page).to_not have_content 'Empty'
  end
end
