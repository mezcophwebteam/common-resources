Feature: Admin User Manipulates Person's Research Areas
  As an Admin User
  I need to be able to assign people their research areas
  So I can see what they're interested in
  And, later, relate them to research projects and each other

  Background:
    Given I am an admin user
    And I am logged in
    And I have a person

  Scenario: Add to a person's research areas
    Given I am at the "/admin/people/1/edit" path
    When I check the research area "Disease Management"
    And I check the research area "Injury Prevention"
    And I check the research area "Genetics"
    And I click the "Update Person" button
    Then I should see research areas
    And I should see the "Disease Management" research area
    And I should see the "Injury Prevention" research area
    And I should see the "Genetics" research area

  Scenario: Remove a person's research areas
    Given my person has the research area "Global Health"
    And my person has the research area "Rural Health"
    And I am at the "/admin/people/1/edit" path
    When I uncheck the research area "Rural Health"
    And I click the "Update Person" button
    Then I should see research areas
    And I should see the "Global Health" research area
    But I should not see the "Rural Health" research area
