= README =

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: Developed on 2.1.5p237, probably compatible with a much larger
range of versions.

* System dependencies: Postgresql for 'pg' gem and databases. You can change
these to whatever you want.

* Configuration: I actually don't know yet

* Database creation: Set up the file in config/database.yml according to your
database setup. You may need to create the databases beforehand depending on
your database user's permissions. Google "rails database.yml" for more
information on the directives.  

* Database initialization: Run 'rake db:setup' to set up databases. Don't forget
the RAILS\_ENV=production part if you're setting it up as a production
application instead of developing it.

* How to run the test suite: Run 'rake test' to run the test suite. If you're
actively developing, run 'guard' to have guard run the tests for you
automatically on each filesystem change.

* Services (job queues, cache servers, search engines, etc.): We really want
this application to connect to EDS via LDAP so it can pull (and update) default
records. This is not yet implemented.

* Deployment instructions: Not yet known. It could be that you just need to set
up a server and unicorn in production and then let off the brakes.

* CAS integration: We really want CAS integration for this application for the
purposes of authentication. This will require some additional manual set up for
each production installation though.
