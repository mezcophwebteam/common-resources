FactoryGirl.define do
  factory :address_for_awochna, class: Address do
    street_address '1295 N. Martin Ave.'
    building_name 'Drachman Hall'
    room_number 'A321'
    city 'Tucson'
    state 'AZ'
    zip '85724'
    phone '5206263738'
    primary true
  end

  factory :address_for_jehiri, class: Address do
    street_address '1295 N. Martin Ave.'
    building_name 'Drachman Hall'
    room_number 'A256'
    po_box 'Campus PO Box: 845209'
    city 'Tucson'
    state 'AZ'
    zip '85724'
    phone '5206261355'
    primary true
  end

  factory :random_address, class: Address do
    street_address { Faker::Address.street_address }
    building_name { Faker::Name.last_name + ' building' }
    room_number { Faker::Address.building_number }
    po_box { "PO Box: " + Faker::Number.number(6) }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    zip { Faker::Address.zip }
    phone { Faker::PhoneNumber.phone_number }
    primary true
  end

end
