FactoryGirl.define do
  factory :awochna, class: Person do
    netid 'awochna'
    first_name 'Alexander'
    last_name 'Wochna'
    preferred_first 'Alex'
    preferred_last 'Wynter'
    employee_type 'Staff'
    email 'awochna@email.arizona.edu'
    primary_title 'Website Designer/Developer'
    after(:create) do |awochna|
      awochna.addresses.create(FactoryGirl.attributes_for(:address_for_awochna))
      awochna.division = Division.find_by(name: 'Administration')
      # Creating a division association doesn't save automatically
      awochna.save
    end
  end

  factory :jehiri, class: Person do
    netid 'jehiri'
    first_name 'John'
    last_name 'Ehiri'
    credentials 'PhD, MPH, MSc'
    employee_type 'Faculty'
    biography  "Dr. Ehiri is a Director and Professor, Division of Health Promotion Sciences, Mel and Enid Zuckerman College of Public Health (MEZCOPH), University of Arizona. Dr. Ehiri's research and teaching focus on social and behavioral aspects of disease prevention, and on a global maternal, child and adolescent health. Most recently, he has focused on HIV prevention, in keeping with its huge global importance."
    email 'jehiri@email.arizona.edu'
    curriculum_vitae 'https://publichealth.arizona.edu/sites/publichealth.arizona.edu/files/EHIRICV2011_0.pdf'
    photo 'https://publichealth.arizona.edu/sites/publichealth.arizona.edu/files/Ehiri_John_0.jpg'
    primary_title 'Division Director'
    after(:create) do |jehiri|
      jehiri.addresses.create(FactoryGirl.attributes_for(:address_for_jehiri))
      # Creates a new title record automatically
      jehiri.titles.create(title: 'Professor')
      # Adds a new record in the join table automatically
      jehiri.research_areas << ResearchArea.find_by(name: 'Behavioral Health')
      jehiri.research_areas << ResearchArea.find_by(name: 'Global Health')
      jehiri.research_areas << ResearchArea.find_by(name: 'Health of Women and Children')
      jehiri.research_areas << ResearchArea.find_by(name: 'Health Promotion')
      jehiri.research_areas << ResearchArea.find_by(name: 'Maternal and Child Health')
      jehiri.research_areas << ResearchArea.find_by(name: 'Native American Health')
      jehiri.sections << Section.find_by(name: 'Global Health Institute')
      # Does not automatically save jehiri
      jehiri.division = Division.find_by(name: 'Health Promotion Sciences')
      jehiri.primary_section = Section.find_by(name: 'Family and Child Health')
      jehiri.save
    end
  end

  factory :random_person, class: Person do
    netid { Faker::Internet.user_name }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    employee_type { ['AP', 'Staff', 'Faculty', 'Joint Faculty', 'Adjunct Faculty'].sample }
    biography { Faker::Lorem.paragraph }
    email { Faker::Internet.safe_email }
    primary_title { Faker::Name.title }
    after(:create) do |person|
      person.addresses.create(FactoryGirl.attributes_for(:random_address))
      person.titles.create(title: "#{Faker::Name.title}")

      # Generates a random number (max 10) of random research areas
      research_area_ids = ResearchArea.all.collect { |area| area.id }
      research_areas = research_area_ids.sample(rand(10))
      research_areas.each do |research_area_id|
        person.research_areas << ResearchArea.find(research_area_id)
      end

      # Generates a random number (max 3) of random sections
      section_ids = Section.all.collect { |section| section.id }
      sections = section_ids.sample(rand(3))
      sections.each do |section_id|
        person.sections << Section.find(section_id)
      end
      person.primary_section = Section.find(section_ids.sample)

      # Generates a random division for the person
      division_ids = Division.all.collect { |division| division.id }
      person.division = Division.find(division_ids.sample)
      
      person.save
    end
  end
end
