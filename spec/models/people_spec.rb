require 'rails_helper'

RSpec.describe Person do

  let (:awochna) { FactoryGirl.create(:awochna) }
  let (:jehiri) { FactoryGirl.create(:jehiri) }

  describe "should respond to some basic person attributes" do

    subject { awochna }

    it { should respond_to :first_name }
    it { should respond_to :last_name }
    it { should respond_to :netid }
    it { should respond_to :preferred_first }
    it { should respond_to :preferred_last }
    it { should respond_to :division }
    it { should respond_to :employee_type }
    it { should respond_to :primary_title }
    it { should respond_to :name }
    it { should respond_to :email }
    it { should respond_to :titles }
    it { should respond_to :primary_address }
    it { should respond_to :addresses }
    it { should respond_to :research_areas }
    it { should respond_to :phone }
    it { should respond_to :street_address }
    it { should respond_to :building_name }
    it { should respond_to :room_number }
    it { should respond_to :po_box }
    it { should respond_to :city }
    it { should respond_to :state }
    it { should respond_to :zip }
    it { should respond_to :primary_section }
    it { should respond_to :sections }
    it { should respond_to :biography }
    it { should respond_to :curriculum_vitae }
    it { should respond_to :photo }

  end

  describe "should be invalid" do

    specify "without a NetID" do
      awochna.netid = ''
      expect(awochna).to_not be_valid
    end

    specify "without a First Name" do
      awochna.first_name = ''
      expect(awochna).to_not be_valid
    end

    specify "without a Last Name" do
      awochna.last_name = ''
      expect(awochna).to_not be_valid
    end

    specify "without a proper employee type" do
      awochna.employee_type = 'employee'
      expect(awochna).to_not be_valid
    end
  end

  describe "has some attributes dynamically composed, like" do

    specify "a name" do
      expect(awochna.name).to eq "#{awochna.preferred_first} #{awochna.preferred_last}"
    end
    
    specify "a street address" do
      expect(awochna.street_address).to eq awochna.primary_address.street_address
    end

    specify "a PO box" do
      expect(jehiri.po_box).to eq jehiri.primary_address.po_box
    end

    specify "a city" do
      expect(awochna.city).to eq awochna.primary_address.city
    end

    specify "a building name" do
      expect(awochna.building_name).to eq awochna.primary_address.building_name
    end

    specify "a room number" do
      expect(awochna.room_number).to eq awochna.primary_address.room_number
    end

    specify "a state" do
      expect(awochna.state).to eq awochna.primary_address.state
    end

    specify "a zip code" do
      expect(awochna.zip).to eq awochna.primary_address.zip
    end
  end

  describe "has a dynamially composed preferred name" do

    specify "using standard values if no preferred ones" do
      awochna.preferred_first = ''
      awochna.preferred_last = ''
      expect(awochna.name).to eq "#{awochna.first_name} #{awochna.last_name}"
    end

    specify "using one standard value and one preferred if given" do
      awochna.preferred_last = ''
      expect(awochna.name).to eq "#{awochna.preferred_first} #{awochna.last_name}"
    end
  end

  it "can have a title added" do
    awochna.titles.create(title: "Linux System Administrator")
    expect(awochna.titles).to include Title.find_by(title: "Linux System Administrator")
  end

  context "can have have addresses" do
    let (:random_person) { FactoryGirl.create(:random_person) }

    describe "when having 2 addresses" do
      before do
        random_person.addresses.create(FactoryGirl.attributes_for(:random_address,
                                                                  primary: false))
      end
      
      it "should have one primary address marked" do
        expect(random_person.addresses.count).to eq 2
        expect(random_person.addresses.where(primary: true).count).to eq 1
      end

      describe "the primary address should be available as attributes on the person" do
        before do
          @primary_address = random_person.addresses.where(primary: true).take
        end

        it "#primary_address" do
          expect(random_person.primary_address).to eq @primary_address
        end

        it "#street_address" do
          expect(random_person.street_address).to eq @primary_address.street_address
        end

        it "#po_box" do
          expect(random_person.po_box).to eq @primary_address.po_box
        end

        it "#city" do
          expect(random_person.city).to eq @primary_address.city
        end

        it "#building_name" do
          expect(random_person.building_name).to eq @primary_address.building_name
        end
        
        it "#room_number" do
          expect(random_person.room_number).to eq @primary_address.room_number
        end

        it "#state" do
          expect(random_person.state).to eq @primary_address.state
        end

        it "#zip" do
          expect(random_person.zip).to eq @primary_address.zip
        end

        it "#phone" do
          expect(random_person.phone).to eq @primary_address.phone
        end
      end
      
      context "when no primary address is marked, it" do
        before do
          random_person.addresses.each do |addr|
            addr.primary = false
            addr.save 
          end
        end

        it "should have no primary address" do
          expect(random_person.primary_address).to eq nil
        end

        describe "should have empty values for" do

          it "#street_address" do
            expect(random_person.street_address).to eq ''
          end

          it "#po_box" do
            expect(random_person.po_box).to eq ''
          end

          it "#city" do
            expect(random_person.city).to eq ''
          end

          it "#building_name" do
            expect(random_person.building_name).to eq ''
          end

          it "#room_number" do
            expect(random_person.room_number).to eq ''
          end

          it "#state" do
            expect(random_person.state).to eq ''
          end
          it "#zip" do
            expect(random_person.zip).to eq ''
          end
          
          it "#phone" do
            expect(random_person.phone).to eq ''
          end
        end
      end
    end
  end
end
