require 'rails_helper'

RSpec.describe "Person's Titles" do

  let!(:jehiri) { FactoryGirl.create(:jehiri) }
  let(:professor) { jehiri.titles.find_by(title: 'Professor') }

  subject { jehiri.titles }

  it "should have a title" do
    expect(subject.count).to eq 1
  end

  describe "can have a new title added to it" do

    specify "which is created just for them" do
      expect do
        subject.create(title: 'Researcher')
      end.to change{Title.count}.by(1)
    end

    specify "which increases the title count for that person" do
      expect do
        subject.create(title: 'Researcher')
      end.to change{subject.count}.by(1)
    end
  end

  describe "can have a title removed" do

    specify "which also destroys the title completely" do
      expect do
        subject.destroy(professor)
      end.to change{Title.count}
    end

    specify "which decreases the title count for that person" do
      expect do
        subject.destroy(professor)
      end.to change{subject.count}
    end
  end
end
