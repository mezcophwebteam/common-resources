require 'rails_helper'

RSpec.describe "Person Has a Division that" do

  let(:jehiri) { FactoryGirl.create(:jehiri) }

  subject { jehiri.division }

  it "should exist" do
    expect(subject).to_not eq nil
  end

  it "should be changeable" do
    jehiri.division = Division.find_by(name: 'Administration')
    jehiri.save
    expect(jehiri.division.name).to eq 'Administration'
  end

  it "should be valid" do
    expect do
      jehiri.division = Division.find_by!(name: 'Research')
    end.to raise_error
  end
end
