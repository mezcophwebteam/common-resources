require 'rails_helper'

RSpec.describe "Person Has Sections that" do

  let(:jehiri) { FactoryGirl.create(:jehiri) }

  subject { jehiri.sections }

  it "should exist" do
    expect(subject).to_not eq nil
  end

  it "should be valid" do
    expect do
      jehiri.sections << Section.find_by(name: 'Research')
    end.to raise_error
  end

  it "can be added to" do
    expect do
      jehiri.sections << Section.find_by(name: 'Biostatistics')
    end.to change{jehiri.sections.count}.by(1)
  end

  it "can have sections removed" do
    expect do
      jehiri.sections.delete(jehiri.sections.first)
    end.to change{jehiri.sections.count}.by(-1)
  end

  it "cannot change through removal" do
    expect do
      jehiri.sections.delete(jehiri.sections.first)
    end.to_not change{Section.count}
  end
end
