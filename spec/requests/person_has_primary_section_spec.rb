require 'rails_helper'

RSpec.describe "Person Has a Primary Section that" do

  let(:jehiri) { FactoryGirl.create(:jehiri) }

  it "is valid" do
    expect(jehiri.primary_section).to_not eq nil
  end

  it "cannot be invalid" do
    expect do
      jehiri.primary_section = Section.find_by!(name: 'Research')
    end.to raise_error
  end

  it "can be empty" do
    jehiri.primary_section = nil
    expect(jehiri).to be_valid
  end
end
