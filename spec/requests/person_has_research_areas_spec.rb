require 'rails_helper'

RSpec.describe "Person's Research Areas" do

  let(:jehiri) { FactoryGirl.create(:jehiri) }
  let(:global_health) { ResearchArea.find_by(name: 'Global Health') }
  let(:rural_health) { ResearchArea.find_by(name: 'Rural Health') }

  subject { jehiri.research_areas }

  it "should have multiple" do
    expect(jehiri.research_areas.count).to be > 1
  end

  describe "can have a new area added to it" do

    specify "which increases the number of research areas that person has" do
      expect do
        subject << rural_health
      end.to change{subject.count}.by(1)
    end

    specify "but it does not increase the total number of research areas" do
      expect do
        subject << rural_health
      end.to_not change{ResearchArea.count}
    end
  end

  describe "can have an area deleted from it" do

    specify "which decreases the number of research areas that person has" do
      expect do
        subject.destroy(global_health)
      end.to change{subject.count}.by(-1)
    end

    specify "but it does not decrease the total number of research areas" do
      expect do
        subject.destroy(global_health)
      end.to_not change{ResearchArea.count}
    end
  end
end
