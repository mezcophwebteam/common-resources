require 'rails_helper'

RSpec.describe 'People Directory API' do

  let!(:awochna) { FactoryGirl.create(:awochna) }
  let!(:jehiri) { FactoryGirl.create(:jehiri) }

  describe "results in a not found error" do

    specify "for posting person data" do
      post '/people', person: FactoryGirl.attributes_for(:awochna)
      expect(response.status).to eq 404
    end

    specify "for requesting a new person form" do
      get '/people/new'
      expect(response.status).to eq 404
    end

    specify "for trying to edit a person" do
      get '/people/awochna/edit'
      expect(response.status).to eq 404
    end

    specify "for trying to delete a person" do
      delete '/people/awochna'
      expect(response.status).to eq 404
    end
  end

  it "will not create a person with post data" do
    expect do
      post '/people', person: FactoryGirl.attributes_for(:awochna)
    end.to_not change { Person.count }
  end

  it "will accept a NetID perfectly fine" do
    get "/people/#{awochna.netid}"
    expect(response.status).to eq 200
  end

  it "will not accept some arbitrary value, like a name" do
    get "/people/#{awochna.last_name}"
    expect(response.status).to eq 404
  end

  it "will not throw an error if the person doesn't have an address" do
    awochna.addresses.first.destroy
    expect { get '/people/awochna' }.to_not raise_error
  end

  describe "will give back some json data which" do
    before { get '/people/jehiri' }

    subject { JSON.parse(response.body, symbolize_names: true) }

    it { is_expected.to_not include :id }
    it { is_expected.to_not include :created_at }
    it { is_expected.to_not include :updated_at }
    it { is_expected.to_not include :middle_initial }
    it { is_expected.to_not include :employee_type }
    it { is_expected.to include :netid }
    it { is_expected.to include :name }
    it { is_expected.to include :phone }
    it { is_expected.to include :building_name }
    it { is_expected.to include :city }
    it { is_expected.to include :po_box }
    it { is_expected.to include :room_number }
    it { is_expected.to include :state }
    it { is_expected.to include :zip }
    it { is_expected.to include :titles }
    it { is_expected.to include :research_areas }
    it { is_expected.to include :division }
    it { is_expected.to include :sections }
    it { is_expected.to include :primary_section }
    it { is_expected.to include :last_name }
    it { is_expected.to include :first_name }
    it { is_expected.to include :preferred_first }
    it { is_expected.to include :preferred_last }
    it { is_expected.to include :credentials }
    it { is_expected.to include :center }
    it { is_expected.to include :biography }
    it { is_expected.to include :email }
    it { is_expected.to include :curriculum_vitae }
    it { is_expected.to include :photo }
    it { is_expected.to include :website }
    it { is_expected.to include :addresses }
    it { is_expected.to_not include :primary_address }

    describe "has nested address attributes" do

      subject { JSON.parse(response.body, symbolize_names: true)[:addresses][0] }

      it { is_expected.to include :street_address }
      it { is_expected.to include :po_box }
      it { is_expected.to include :building_name }
      it { is_expected.to include :room_number }
      it { is_expected.to include :city }
      it { is_expected.to include :state }
      it { is_expected.to include :zip }
      it { is_expected.to include :phone }
      it { is_expected.to_not include :id }
      it { is_expected.to_not include :person_id }
      it { is_expected.to_not include :created_at }
      it { is_expected.to_not include :updated_at }
    end

    describe "has titles that" do

      subject { JSON.parse(response.body, symbolize_names: true)[:titles][0] }

      it { is_expected.to include :title }
      it { is_expected.to_not include :id }
      it { is_expected.to_not include :person_id }
      it { is_expected.to_not include :created_at }
      it { is_expected.to_not include :updated_at }
    end

    describe "has sections that" do

      subject { JSON.parse(response.body, symbolize_names: true)[:sections][0] }

      it { is_expected.to include :name }
      it { is_expected.to_not include :id }
      it { is_expected.to_not include :person_id }
      it { is_expected.to_not include :created_at }
      it { is_expected.to_not include :updated_at }
    end

    describe "has research areas that" do

      subject { JSON.parse(response.body, symbolize_names: true)[:research_areas][0] }

      it { is_expected.to include :name }
      it { is_expected.to_not include :id }
      it { is_expected.to_not include :person_id }
      it { is_expected.to_not include :created_at }
      it { is_expected.to_not include :updated_at }
    end
  end

  describe "can give an index which" do

    before { get '/people' }

    subject { JSON.parse(response.body, symbolize_names: true) }

    it "should include the json for awochna and jehiri" do
      subject.each do |json_person|
        if json_person[:netid] == 'awochna'
          expect(json_person[:last_name]).to eq awochna.last_name
        elsif json_person[:netid] == 'jehiri'
          expect(json_person[:biography]).to eq jehiri.biography
        end
      end
    end
  end
end
