# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

RESEARCH_AREAS ||= ['Aging', 'Behavioral Health', 'Biostatistics',
                   'Border Health', 'Cancer', 'Cardiovascular Disease',
                   'Disease Management', 'Disease Prevention',
                   'Diabetes', 'Emergency Preparedness',
                   'Environmental Health', 'Epidemiology', 'Genetics',
                   'Global Health', 'Health Disparities',
                   'Health of Women and Children', 'Health Promotion',
                   'Hispanic Health', 'Infectious Disease',
                   'Injury Prevention', 'Integrative Medicine',
                   'Maternal and Child Health', 'Native American Health',
                   'Nutrition & Diet', 'Obesity', 'Occupational Health',
                   'Planning and Evaluation',
                   'Public Health Informatics',
                   'Public Health Interventions',
                   'Public Health Preparedness', 'Public Health Policy',
                   'Respiratory Disease', 'Rural Health',
                   'School Health', 'Tobacco and Substance Abuse',
                   'Women, Families, and Children']

DIVISIONS ||= ['Administration', 'Community, Environment & Policy',
               'Epidemiology and Biostatistics',
               'Health Promotion Sciences', 'Phoenix Campus']

SECTIONS ||= ['Biostatistics', 'Environmental Health Sciences',
              'Epidemiology', 'Family and Child Health',
              'Health Behavior Health Promotion',
              'Public Health Policy and Managemnt',
              'Public Health Practice', 'Global Health Institute',
              'Health Services Administration']

ResearchArea.delete_all

RESEARCH_AREAS.each do |research_area|
  ResearchArea.create(name: research_area)
end

Division.delete_all

DIVISIONS.each do |division|
  Division.create(name: division)
end

Section.delete_all

SECTIONS.each do |section|
  Section.create(name: section)
end

