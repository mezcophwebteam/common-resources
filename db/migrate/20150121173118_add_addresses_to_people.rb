class AddAddressesToPeople < ActiveRecord::Migration
  def change
    change_table :people do |t|
      t.string :tucson_phone
      t.string :tucson_street_address
      t.string :tucson_building_name
      t.string :tucson_room_number
      t.string :tucson_po_box
      t.string :tucson_city
      t.string :tucson_state
      t.string :tucson_zip

      t.string :phoenix_phone
      t.string :phoenix_street_address
      t.string :phoenix_building_name
      t.string :phoenix_room_number
      t.string :phoenix_po_box
      t.string :phoenix_city
      t.string :phoenix_state
      t.string :phoenix_zip
    end
  end
end
