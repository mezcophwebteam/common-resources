class AddSectionsDivisionsToPeople < ActiveRecord::Migration
  def change
    remove_column :people, :secondary_sections
    remove_column :people, :division

    add_column :people, :division_id, :integer

    create_table :people_sections do |t|
      t.belongs_to :person, index: true
      t.belongs_to :section, index: true
    end
  end
end
