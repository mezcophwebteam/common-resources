class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :netid
      t.string :first_name
      t.string :last_name
      t.string :preferred_first
      t.string :preferred_last
      t.string :credentials
      t.string :middle_initial
      # Making titles a 'text' column allows it to be a serialize array for
      # those faculty with multiple titles.
      t.text :titles
      t.string :division
      t.string :primary_section
      t.text :secondary_sections # Can be multiple
      t.string :employee_type
      t.text :center # Can be multiple
      t.text :biography # Can be long
      t.text :research_areas # Serialized array = multiple
      t.string :email
      t.string :curriculum_vitae # Just a string giving the URL
      t.string :photo # Also just a string URL to the resource
      t.string :website
      t.string :primary_title

      t.timestamps null: false

      t.index :netid
    end
  end
end
