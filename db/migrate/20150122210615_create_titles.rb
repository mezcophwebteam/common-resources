class CreateTitles < ActiveRecord::Migration
  def change
    create_table :titles do |t|
      t.string :title
      t.belongs_to :person, index: true

      t.timestamps null: false
    end
  end
end
