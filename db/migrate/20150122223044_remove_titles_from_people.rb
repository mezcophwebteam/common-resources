class RemoveTitlesFromPeople < ActiveRecord::Migration
  def change
    remove_column :people, :titles
  end
end
