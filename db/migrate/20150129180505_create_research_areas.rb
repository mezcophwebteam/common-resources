class CreateResearchAreas < ActiveRecord::Migration
  def change
    create_table :research_areas do |t|
      t.string :name

      t.timestamps null: false
    end

    remove_column :people, :research_areas

    create_table :people_research_areas do |t|
      t.belongs_to :person, index: true
      t.belongs_to :research_area, index: true
    end
  end
end
