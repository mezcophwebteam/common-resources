class AddPrimarySectionToPerson < ActiveRecord::Migration
  def change
    remove_column :people, :primary_section
    add_column :people, :primary_section_id, :integer
  end
end
