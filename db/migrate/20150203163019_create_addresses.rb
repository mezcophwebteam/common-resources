class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :street_address
      t.string :po_box
      t.string :building_name
      t.string :room_number
      t.string :city
      t.string :state
      t.string :zip
      t.string :phone
      t.boolean :primary

      t.timestamps null: false
    end
  end
end
