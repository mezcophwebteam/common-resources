class AddAddressesToPeopleAsRelationship < ActiveRecord::Migration
  def change
    change_table :people do |t|
      t.remove :tucson_street_address
      t.remove :tucson_po_box
      t.remove :tucson_building_name
      t.remove :tucson_room_number
      t.remove :tucson_city
      t.remove :tucson_state
      t.remove :tucson_zip
      t.remove :tucson_phone
      t.remove :phoenix_street_address
      t.remove :phoenix_po_box
      t.remove :phoenix_building_name
      t.remove :phoenix_room_number
      t.remove :phoenix_city
      t.remove :phoenix_state
      t.remove :phoenix_zip
      t.remove :phoenix_phone
    end

    change_table :addresses do |t|
      t.belongs_to :person, index: true
    end
  end
end
