# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150203163241) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.string   "street_address"
    t.string   "po_box"
    t.string   "building_name"
    t.string   "room_number"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.boolean  "primary"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "person_id"
  end

  add_index "addresses", ["person_id"], name: "index_addresses_on_person_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "divisions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "netid"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "preferred_first"
    t.string   "preferred_last"
    t.string   "credentials"
    t.string   "middle_initial"
    t.string   "employee_type"
    t.text     "center"
    t.text     "biography"
    t.string   "email"
    t.string   "curriculum_vitae"
    t.string   "photo"
    t.string   "website"
    t.string   "primary_title"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "division_id"
    t.integer  "primary_section_id"
  end

  add_index "people", ["netid"], name: "index_people_on_netid", using: :btree

  create_table "people_research_areas", force: :cascade do |t|
    t.integer "person_id"
    t.integer "research_area_id"
  end

  add_index "people_research_areas", ["person_id"], name: "index_people_research_areas_on_person_id", using: :btree
  add_index "people_research_areas", ["research_area_id"], name: "index_people_research_areas_on_research_area_id", using: :btree

  create_table "people_sections", force: :cascade do |t|
    t.integer "person_id"
    t.integer "section_id"
  end

  add_index "people_sections", ["person_id"], name: "index_people_sections_on_person_id", using: :btree
  add_index "people_sections", ["section_id"], name: "index_people_sections_on_section_id", using: :btree

  create_table "research_areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sections", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "titles", force: :cascade do |t|
    t.string   "title"
    t.integer  "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "titles", ["person_id"], name: "index_titles_on_person_id", using: :btree

end
