// Directory API js
// Front end script

$(document).ready(function() {
  var netid = $('div').data('netid');
  var url = "http://localhost:3000/people/" + netid;
  $.getJSON(url, function(response){
    var parentElement = $('[data-directory]');
    parentElement.find('.name').html(response.name);
    parentElement.find('.primary_title').html(response.primary_title);
  });
});
