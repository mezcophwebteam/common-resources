class PeopleController < ApplicationController

  def create
    render nothing: true, status: :not_found
  end

  def new
    render nothing: true, status: :not_found
  end

  def edit
    render nothing: true, status: :not_found
  end

  def destroy
    render nothing: true, status: :not_found
  end

  def show
    @person = Person.find_by(netid: params[:id])
    if @person
      render json: @person
    else
      render nothing: true, status: :not_found
    end
  end

  def index
    people = Person.all
    render json: people
  end
end
