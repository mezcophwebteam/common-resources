ActiveAdmin.register Person do

  controller do
    def scoped_collection
      super.includes :titles
    end
  end

  VALID_EMPLOYEE_TYPES ||= ['AP', 'Staff', 'Faculty', 'Joint Faculty',
                          'Adjunct Faculty']

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  
  permit_params do
    params = [:netid, :first_name, :last_name, :preferred_first,
              :preferred_last, :primary_title, :division, :primary_section_id,
              :employee_type, :biography, :email, :curriculum_vitae, :photo]
    params.push(titles_attributes: [:title, :person_id, :_destroy, :id])
    params.push(section_ids: [])
    params.push(:division_id)
    params.push(research_area_ids: [])
    params.push(addresses_attributes: [:street_address, :po_box, :building_name, 
                                       :room_number, :city, :state, :zip, :phone,
                                       :primary, :_destroy, :id, :person_id])
    params
  end

  form do |f|
    f.inputs do
      f.input :netid
      f.input :first_name
      f.input :last_name
      f.input :preferred_first
      f.input :preferred_last
      f.input :email
      f.input :primary_title
      f.inputs do
        f.has_many :titles, allow_destroy: true, heading: 'Titles' do |t|
          t.input :title
          t.actions
        end
      end
      f.input :division, as: :select, collection: Division.all
      f.input :primary_section, as: :select, collection: Section.all
      f.input :sections, as: :check_boxes, collection: Section.all
      f.input :employee_type, as: :select, collection: VALID_EMPLOYEE_TYPES
      f.input :biography
      f.input :research_areas, as: :check_boxes, collection: ResearchArea.all
      f.input :curriculum_vitae
      f.inputs do
        f.has_many :addresses, allow_destroy: true,
                               heading: 'Addresses' do |addr|
          addr.input :street_address
          addr.input :po_box
          addr.input :building_name
          addr.input :room_number
          addr.input :city
          addr.input :state
          addr.input :zip
          addr.input :phone
          addr.input :primary
          addr.actions
        end
      end
    end

    f.inputs do
      f.actions
    end
  end

  index do
    selectable_column
    column :id
    column :netid
    column :first_name
    column :last_name
    column :primary_title
    column :titles
    column :division
    column :employee_type
    actions
  end

  show do
    attributes_table do
      row :id
      row :netid
      row :name do
        person.name
      end
      row :credentials
      row :primary_title
      row :titles do
        title_list = []
        person.titles.each do |title|
          title_list << title.title
        end
        title_list.join(', ')
      end
      row :division
      row :primary_section
      row :sections do
        sections_list = []
        person.sections.each do |section|
          sections_list << section.name
        end
        sections_list.join(', ')
      end
      row :employee_type
      row :email
      row :center
      row :research_areas do
        research_areas_list = []
        person.research_areas.each do |research_area|
          research_areas_list << research_area.name
        end
        research_areas_list.join(', ')
      end
      row :biography
      row :curriculum_vitae
      row :photo do
        image_tag person.photo
      end
      row :website
      row :addresses do
        person.addresses.each do |address|
          attributes_table_for address do
            row :street_address
            row :po_box
            row :building_name
            row :room_number
            row :city
            row :state
            row :zip
            row :phone
          end
        end
      end
    end
    active_admin_comments
  end

end
