class Person < ActiveRecord::Base

  VALID_EMPLOYEE_TYPES ||= ['AP', 'Staff', 'Faculty', 'Joint Faculty',
                          'Adjunct Faculty']


  has_many :titles, dependent: :destroy
  accepts_nested_attributes_for :titles, allow_destroy: true
  has_and_belongs_to_many :research_areas
  has_and_belongs_to_many :sections
  belongs_to :primary_section, class_name: 'Section'
  belongs_to :division
  has_many :addresses, dependent: :destroy
  accepts_nested_attributes_for :addresses, allow_destroy: true

  validates :netid, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true

  validates :employee_type, inclusion: { in: VALID_EMPLOYEE_TYPES }

  def name
    first = self.preferred_first.to_s == '' ? self.first_name : self.preferred_first
    last = self.preferred_last.to_s == '' ? self.last_name : self.preferred_last
    "#{first} #{last}"
  end

  def primary_address
    self.addresses.where(primary: true).take
  end

  def street_address
    primary_address ? primary_address.street_address : ''
  end

  def po_box
    primary_address ? primary_address.po_box : ''
  end

  def city
    primary_address ? primary_address.city : ''
  end

  def building_name
    primary_address ? primary_address.building_name : ''
  end

  def room_number
    primary_address ? primary_address.room_number : ''
  end

  def state
    primary_address ? primary_address.state : ''
  end

  def zip
    primary_address ? primary_address.zip : ''
  end

  def phone
    primary_address ? primary_address.phone : ''
  end

  def as_json(options={})
    super(only: [:biography, :center, :credentials, :curriculum_vitae, :email,
                 :first_name, :last_name, :netid, :photo, :preferred_first,
                 :preferred_last, :primary_title, :website],
          methods: [:name, :street_address, :po_box, :city, :building_name,
                    :room_number, :state, :zip, :phone],
          include: {
                      titles: { only: [:title] },
                      research_areas: { only: [:name] },
                      division: { only: [:name] },
                      sections: { only: [:name] },
                      primary_section: { only: [:name] },
                      addresses: { only: [:street_address, :po_box, :city,
                                          :building_name, :room_number, :state,
                                          :zip, :phone] }
                    }
         )
  end
end
